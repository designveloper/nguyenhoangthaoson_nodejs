var inventoryData = require('../data/inventory');
var warehouse = require('./warehouse');

function findItem(name) {
    var i = inventoryData.map(item => item.name).indexOf(name);
    if (i === -1) {
        console.log(`Item - ${name} not found`);
        return null;
    } else {
        return inventoryData[i];
    }
}

function isInStock(name, quantity) {
    var item = findItem(name);
    return item && item.quantity >= quantity;
}

function order(name, quantity, complete) {
    complete = complete || function() {};
    if (isInStock(name, quantity)) {
        console.log(`ordering ${quantity} of item # ${name}`);
        warehouse.packageAndShip(name, quantity, function(tracking) {
            console.log(`order shipped, tracking - ${tracking}`);
            complete(tracking);
        });
        return true;
    } else {
        console.log(`there are not enough ${quantity} of item '${name}' in stock`);
        return false;
    }
}

module.exports.orderItem = order;