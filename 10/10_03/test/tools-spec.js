var expect = require("chai").expect;
var tools = require("../lib/tools");
var nock = require("nock");

describe("Tools", function() {

    describe("printName()", function() {
        it("should print the last name first", function() {
            var results = tools.printName({ first: "Son", last: "Nguyen" });
            expect(results).to.equal("Nguyen Son");
        });
    });

    describe("loadWiki()", function() {

        before(function() {

            nock("https://dota2.gamepedia.com")
                .get("/Invoker")
                .reply(200, "Injoker");

        });

        it("Load doto 2 wikipedia page", function(done) {

            tools.loadWiki({ name: "Invoker" }, function(html) {
                expect(html).to.equal("Injoker");
                done();
            });

        });

    });

});