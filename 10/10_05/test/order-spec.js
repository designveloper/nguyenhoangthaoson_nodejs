var expect = require("chai").expect;
var rewire = require("rewire");

var order = rewire("../lib/order");

var sinon = require("sinon");

describe("Ordering Items", function() {

    beforeEach(function() {

        this.testData = [
            { name: "test item 1", quantity: 10 },
            { name: "test item 2", quantity: 4 },
            { name: "test item 3", quantity: 6 },
            { name: "test item 4", quantity: 12 }
        ];


        this.console = {
            log: sinon.spy()
        };

        order.__set__("inventoryData", this.testData);
        order.__set__("console", this.console);

    });

    it("order an item when there are enough in stock", function(done) {

        var _this = this;

        order.orderItem("test item 4", 3, function() {

            expect(_this.console.log.callCount).to.equal(2);

            done();
        });

    });

});