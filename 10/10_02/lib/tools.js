var https = require("https");

module.exports = {

    printName(person) {
        return `${person.last} ${person.first}`;
    },

    loadWiki(hero, callback) {

        var url = `https://dota2.gamepedia.com/Invoker/${hero.name}`;

        https.get(url, function(res) {

            var body = "";

            res.setEncoding("UTF-8");

            res.on("data", function(chunk) {
                body += chunk;
            });

            res.on("end", function() {
                callback(body);
            });
        });

    }

};