var expect = require("chai").expect;
var tools = require("../lib/tools");

describe("Tools", function() {

    describe("printName()", function() {
        it("should print the last name first", function() {
            var results = tools.printName({ first: "Son", last: "Nguyen" });
            expect(results).to.equal("Nguyen Son");
        });
    });

    describe("loadWiki()", function() {

        this.timeout(5000);

        it("Load doto 2 wikipedia page", function(done) {

            tools.loadWiki({ name: "Dagon" }, function(html) {

                expect(html).to.be.ok;
                done();
            });

        });

    });

});