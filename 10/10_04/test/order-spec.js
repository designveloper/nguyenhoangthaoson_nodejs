var expect = require("chai").expect;
var rewire = require("rewire");

var order = rewire("../lib/order");

describe("Ordering Items", function() {

    beforeEach(function() {

        this.testData = [
            { name: "test item 1", quantity: 10 },
            { name: "test item 2", quantity: 4 },
            { name: "test item 3", quantity: 6 },
            { name: "test item 4", quantity: 12 }
        ];

        order.__set__("inventoryData", this.testData);

    });

    it("order an item when there are enough in stock", function(done) {

        order.orderItem("test item 2", 1, function() {
            done();
        });

    });

});