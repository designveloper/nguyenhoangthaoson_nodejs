### 1 What is Node.js
    * History
        - 2009: created
        - 2011: npm created
        - 2014: io.js project forked
        - Sep 14, 2015: Node.js 4.0 released
    * Apache
        - Every request is single thread
        - Have to wait until request finished
        - Blocking
    * Node.js
        - NodeJS is single thread
        - Asynchronously
### 2 Install Node.js
    * Visit https://nodejs.org/en/
### 3 Node core          
    * Global object: global
    * Full path to directory: __dirname
    * Full path to files: __filename
    * require function: to use node_modules
### 4 Node Modules
    * Core modules: Modules installed locally with the installation of NodeJS   
    * Readline: module allow to easily control prompting a user
    * events.EventEmitter: Allows creating listeners for an emit custom event
    * Module.exports: the object that is returned by the require statement. 
    * child process with exec: 
        - run external process from other environment.
        - exec made of asynchronously synchronous processes
        - spawn made for longer, ongoing processes with large amount of data 
### 5 File System        
    * fs: file system module
        - Common action: read files, write/ append files, create, rename, remove files
        - Have both synchronous & asynchronous version
    * Throww error will cause program crash, use log instead
    * Stream: 
        - Asynchronous handling a data flow
        - Readable, Writeable
        - Can work with binary or data encoded

### 6 The HTTP Module           
    * Creates web servers, making requests, handling responses, ect.
        - Common (default) port:
            * HTTP: 80
            * HTTPS: 443
    * There some common object: req,res
        - req: contain all request data
        - res: blank object, values filled depend on user
        - option: json object, contain data to create http server
    * listen method: define which port will server listen to request
### 7 Node Package Manager
    * npm: the tool alow us to install Node package were created by open source community
        - ref: https://www.npmjs.com/    
### 8 Web Servers        
    * Popular framework for developing web server applications.
        - package.json: A manifest that contains info about the app, and keeps track of the app's dependencies
    - Middleware: Customized plugins that adds functionality to the app. Use with app.use()
### 9 Web Socket
    * Allow true 2 way connection between client & server
    * Use their own protocol to send & receive message from TCP
        - Poll: make request to the server to check if the state of the server has changed
        - Long polling: make a request of a server & leave it open with longer period of time
        - Web socket:
            + Connect to server and leave 2 way connection open
    * Socket server:
        - Fail back to long polling when websocket aren't supported        
### 10 Testing & Debugging
    * Popular framework: Mocha, Chai        
    * Mocking a server with Nock
        - Reduce testing time
        - Use mocha's hook ( before() )
    * Injecting dependencies with rewire    
        - Inject user created data instead of read data
    * Advance testing
        - Spies with Sinon: 
            + Record how function is called, what argument called with
            + Can be used in place of real functions to check if a specific function has been invoked.
        - Stub with Sinon
            + More powerful spies
            + Allow controlling behavior of function
        - Code coverage with Istanbul
            + Generates code coverage report 
            + Show how many lines of code has test cover
        - HTTP endpoint test with Supertest
        - Check server response with Cheerio    
### 11 Automation & deployment
    * Grunt: command line interface support automation process
    * Browserify: bundle all dependency into 1 js file    
    * Watch: task run when changed & saved a file
    * Automation with npm: setting command in package.json 
    * Debugging with npm:    
        - Use --inspector when run
        - Use IDE debugging tool: vscode, etc..
        - Use browser debugging tool